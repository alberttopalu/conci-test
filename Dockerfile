FROM ubuntu:16.10
MAINTAINER Alexander Schenkel <alex@alexi.ch>

WORKDIR /var/www/test

RUN apt-get update && \
    apt-get dist-upgrade -y && \
    apt-get install -y \
      apache2 \
      php \
      php-cli \
      libapache2-mod-php \
      php-apcu \
      php-apcu-bc \
      php-gd \
      php-json \
      php-ldap \
      php-mbstring \
      php-mysql \
      php-opcache \
      php-pgsql \
      php-sqlite3 \
      php-xml \
      php-xsl \
      php-zip \
      php-soap \
      php-xdebug \
      composer

COPY docker/apache_default /etc/apache2/sites-available/000-default.conf
COPY docker/run /usr/local/bin/run
RUN chmod +x /usr/local/bin/run
RUN a2enmod rewrite

EXPOSE 8181
CMD ["/usr/local/bin/run"]
