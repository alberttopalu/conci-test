<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 14.02.18
 * Time: 14:49
 */

namespace App\Exceptions;


class ForbiddenException extends \Exception
{
    public function __construct($message = 'Forbidden method for you')
    {
        if ($message) {
            $this->message = $message;
        }
    }

}