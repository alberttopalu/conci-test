<?php

namespace App\Http\Controllers;

use App\Exceptions\ForbiddenException;
use App\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class NotesController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return  response()->json([
            'success' => true,
            'data' => Note::all()
        ]);
    }

    /**
     * @param Note $note
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Note $note)
    {
        return  response()->json([
            'success' => true,
            'data' => $note->toArray()
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $rules = [
            'title' => 'required',
            'description' => 'required',
        ];

        $input = $request->only('title', 'description');
        $validator = Validator::make($input, $rules);

        if($validator->fails()) {
            $error = $validator->messages();

            return response()->json([
                'success'=> false,
                'error'=> $error
            ], 400);
        }

        $user = Auth::user();
        $note = new Note($input);
        $user->notes()->save($note);

        return response()->json([
            'success'=> true,
//            'note' => $note->toArray()
            'note' => $note->id
        ], 201);
    }

    /**
     * @param Request $request
     * @param Note $note
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Note $note)
    {
        try {
            if ($note->user->id != Auth::user()->id){
                throw new ForbiddenException();
            }

            $note->update([
                'title' => $request->title,
                'description' => $request->description
            ]);

        } catch (ForbiddenException $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ], 403);
        }

        return response()->json([
            'success'=> true,
            'note' => $note->id
        ], 201);
    }


}
