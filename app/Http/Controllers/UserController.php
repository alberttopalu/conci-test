<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function profile()
    {
        $user = Auth::user();

        return response()->json([
            'success' => true,
            'data'=> [
                'user' => $user->toArray(),
                'notes' => $user->notes->toArray()
            ]
        ]);
    }
}
