<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'AuthController@register');

Route::post('login', 'AuthController@login');

Route::group(['middleware' => ['jwt.auth']], function() {

    /**
     * User routes
     */
    Route::get('profile', 'UserController@profile');

    Route::get('logout', 'AuthController@logout');


    /**
     * Notes routes
     */
    Route::post('notes', 'NotesController@create');

    Route::post('notes/{note}', 'NotesController@update');

});



Route::get('list', 'NotesController@index');

Route::get('lists/{note}', 'NotesController@show');